If the integer a is a multiple of 6, the integer b is a multiple of 3, and a>b>0, then which of the following integers must be a^2-b^2 multiple of$#6$#8$#9$#10$#9
If a sweater sells for $48 after a 25 percent markdown, what was its original price$#56$#60$#64$#68$#64
If x + y =8 and y- x = -2, then y=$#-2$#3$#5$#8$#10$#3
If there are 14 women and 10 men employed in a certain office, what is the probability that one employee picked at random will be a woman$#1/6$#1/14$#7/12$#1$#7/12
What will be the fraction of 20%$#1/4$#1/5$#1/10$#None$#1/5
What will be the fraction of 4%$#1/20$#1/50$#1/75$#1/25$#1/25
The ratio 5:20 expressed as percent equals to$#50$#125$#25$#None$#25
2.09 can be expressed in terms of percentage as$#2.09$#20.9$#209$#0.209$#209
Half of 1 percent written as decimal is$#5$#0.5$#0.05$#0.005$#0.005$#0.005
What is 15 percent of 34$#5.10$#4.10$#3.10$#2.10$#5.10
Evaluate 28% of 450 + 45% of 280$#232$#242$#252$#262$#252
2 is what percent of 50$#2$#4$#6$#8$#4