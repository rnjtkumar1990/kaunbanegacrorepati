package com.kbc;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Question {

	protected String question="";
	protected int level=0;
	protected String correctAnswer="";
	private static Scanner readFile=null;
	private static ArrayList<Integer> questions=null;
	public Question()
	{
		
	}
	public Question(int level)
	{

		
		this.level=level;
	}

	public String getQuestions()
	{
		String filename="level"+level+".txt";

		try {
			readFile= new Scanner(new File(filename));
		} catch (FileNotFoundException e) {
			System.err.println("file is not present to read question");
			e.printStackTrace();
		}

		String line=readFile.nextLine();
		while(questions.contains(line.hashCode()))
		{
			line=readFile.nextLine();
		}
		questions.add(line.hashCode());
		String ques[]=line.trim().split("\\$#");

		question="";
		question=ques[0]+
				"\n1) "+ques[1]+
				"\n2) "+ques[2]+
				"\n3) "+ques[3]+
				"\n4) "+ques[4];
		for(int i=1;i<5;i++)
		{
			if(ques[i].equalsIgnoreCase(ques[5]))
				correctAnswer=Integer.toString(i);
		}
		return question;
	}
	public void setLevel(int level) {
		this.level = level;
	}

	public  int getLevel()
	{
		return level;
	}
	
	public String correctAnswer()
	{
		return  correctAnswer;
	}
}
