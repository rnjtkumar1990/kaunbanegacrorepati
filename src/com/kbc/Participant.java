package com.kbc;

public class Participant {
	private String name;
	private int age;
	private String phone;
	private int currentLevel=0;
	private int prizeMoney=0;
	public Participant(String name, int age, String phone){
		this.name = name;
		this.age=age;
		this.phone = phone;
	}
	public String getName(){
		return name;
	}
	public int getAge(){
		return age;
	}
	public String getPhone(){
		return phone;
	}	
	
	public void setCurrentLevel(int currentLevel)
	{
		this.currentLevel=currentLevel;
	}
	public int getPrizeMoney() {
		return prizeMoney;
	}
	public void setPrizeMoney(int prizeMoney) {
		this.prizeMoney = prizeMoney;
	}
	public int getCurrentLevel() {
		return currentLevel;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
}
