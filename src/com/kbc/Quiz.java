package com.kbc;

public interface Quiz {
	void addQuestion(Question q);
	Participant registerParticipant(String name, int age, String phone);
	int getCurrentLevel();
	Question getNextQuestion();
	boolean lockAnswer(Question q, String answer);
	int getPrizeMoney();
}
