package com.kbc;

import java.util.ArrayList;
import java.util.Scanner;

public class KaunBanegaCrorepati implements Quiz {
	private static Scanner scan=null;
	private static ArrayList<Participant> partcipant;
	private static String question[]= new String[15];
	private static String answer[]= new String[15];

	private int level=0;
	public KaunBanegaCrorepati()
	{
		scan = new Scanner(System.in);
		partcipant= new ArrayList<Participant>();

	}

	//-----------------------------------------------------------------------------
	public static void main(String[] args) {
		KaunBanegaCrorepati crore=new KaunBanegaCrorepati();
		for(int i=0;i<15;i++)
			crore.addQuestion(new Question(i+1));
		boolean flag;
		String name,phone;
		int age;
		do
		{
			flag=false;
			System.out.println("Enter new Participant name\n\n");
			System.out.println("Enter your Name: ");
			name=scan.nextLine();
			System.out.println("Enter your age: ");
			age=scan.nextInt();
			scan.nextLine();
			System.out.println("Enter your mobile: ");
			phone=scan.nextLine();	
			for(int i=0;i<partcipant.size();i++)
			{
				if(partcipant.get(i).getName().equalsIgnoreCase(name)  && (partcipant.get(i).getAge()==age)  && partcipant.get(i).getPhone().equalsIgnoreCase(phone))
					flag=true;
			}
			if(flag)
				System.out.println("Participant is already present in the list\n\n");
		}while(flag);
		if(!flag)
			partcipant.add(crore.registerParticipant(name, age, phone));
	}

	public void addQuestion(Question q) {

		int level=q.getLevel();
		question[level-1]=q.getQuestions();
		answer[level-1]=q.correctAnswer();
	}
	public Participant registerParticipant(String name, int age, String phone) {

		return new Participant(name, age, phone);
	}
	//------------------------------------------------------------------
	public int getCurrentLevel() {
		return new Question().getLevel();
	}
	public Question getNextQuestion() {
		// TODO Auto-generated method stub
		return null;
	}
	public boolean lockAnswer(Question q, String answer) {
		// TODO Auto-generated method stub
		return false;
	}
	public int getPrizeMoney() {
		// TODO Auto-generated method stub
		return 0;
	}

}
